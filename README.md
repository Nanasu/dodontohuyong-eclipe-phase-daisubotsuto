どどんとふ1.48.17に付属していたものを改造。

* 成功度、失敗度の算出、表示
* エクセレント時にダメージへの修正を表示
* `(` `)`なしで直接目標値に数式が書ける[^1]
* `1D100<=`の略記として`EP`、`EP<=`を使用可能[^2]
* 多言語対応（現状日本語、英語）
* 勇気を使用することによって結果が向上する場合は途中経過にそれを表示

[^1]:例: `1d100<=15*3+10`
[^2]:上記の例を`EP15*3+10`、`EP<=15*3+10`と書ける

####使い方
1. [ダウンロード](https://bitbucket.org/Nanasu/dodontohuyong-eclipe-phase-daisubotsuto/downloads)の「**リポジトリをダウンロードする**」からzipファイルをダウンロードして解凍する
2. zipファイル中の`src_bcdice/diceBot`にある拡張子`.rb`ファイルを**あなたの**どどんとふの`src_bcdice/diceBot`ディレクトリに置く（同名のものがあれば上書き）
3. 必要ならあなたのどどんとふの`src_ruby/config.rb`（またはあなたが置いた`config_local.rb`）を修正し、`エクリプス・フェイズ`ダイスボットをリストに表示させる

####注意
あなたのどどんとふの既存の`EclipsePhase.rb`を上書きし、`EclipsePhase_English.rb`を配置後、（どどんとふのバージョンアップなどで）再び`EclipsePhase.rb`をどどんとふ付属のものに上書きした場合、どどんとふが起動しなくなります、再度このダイスボットを配置し直すか`EclipsePhase_English.rb`を取り除いてください。

####その他
**setPrefixes**ブランチにて、ダイスボットのコマンド定義方法変更に対応、いずれ**master**ブランチにマージの予定。

こちらを使用したい場合はダウンロードの[「ブランチ」タブ](https://bitbucket.org/Nanasu/dodontohuyong-eclipe-phase-daisubotsuto/downloads/?tab=branches)より**setPrefixes**のものをダウンロードしてください（どどんとふ 1.48.28 以降必要）。

####ライセンスとクレジット (License and Credits)

Eclipse Phase is a trademark of Posthuman Studios, LLC. Some Rights Reserved. 
Eclipse Phase products (including printed rulebooks/sourcebooks and PDFs) 
by [Posthuman Studios](http://eclipsephase.com) are licensed under a [Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License](https://creativecommons.org/licenses/by-nc-sa/3.0/), with certain exceptions. 
See [CC licensing page](http://eclipsephase.com/cclicense) on eclipsephase.com for detail. 

日本語ダイスボット中の訳語は『[エクリプス・フェイズ](http://r-r.arclight.co.jp/rpg/eclipsephase)』
（ISBN9784775310229、朱鷺田祐介 監訳、『エクリプス・フェイズ』翻訳チーム 訳、
新紀元社。[書誌は国立国会図書館のもの](http://iss.ndl.go.jp/books/R100000002-I027428941-00)より抜粋）より。

[![88x31.png](https://bitbucket.org/repo/bM89nL/images/2100114126-88x31.png)](https://creativecommons.org/licenses/by-nc-sa/3.0/)

どどんとふ用 Eclipse Phase ダイスボットは、[クリエイティブ・コモンズ 表示 - 非営利 - 継承 3.0 非移植 ライセンス](https://creativecommons.org/licenses/by-nc-sa/3.0/deed.ja)の下に提供されています。